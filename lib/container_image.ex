require EEx

defmodule ContainerImageUtil do

    EEx.function_from_string(:def, :image_name,
        "<%= if registry do %><%= registry %>/<% end %>" <>
        "<%= name %>" <>
        "<%= if tag do %>:<%= tag %><% end %>",
        [:registry, :name, :tag]
    )

end